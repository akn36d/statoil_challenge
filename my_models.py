from keras.models import Sequential
# from keras.models import load_model
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import np_utils

#################################################################
#                             MODEL 1                           #
#################################################################

class stoil_model1_cnn():
    
    def __init__(self):

    	self.name = 'model1_cnn'

    def model(self):
	    # Declaring the model
	    my_model = Sequential()
	    
	    # CNN input layer
	    my_model.add(Conv2D(32,(5,5), activation='relu', input_shape=(75,75,2), padding='same'))
	    # Max pool layer
	    my_model.add(MaxPooling2D(pool_size=(3,3), strides=4))
	    # Dropout for regularization
	    my_model.add(Dropout(0.25))
	    
	    # fully connected dense layers
	    my_model.add(Flatten())
	    my_model.add(Dense(32, activation='relu'))
	    my_model.add(Dropout(0.5))
	    my_model.add(Dense(2, activation='softmax'))
	    return my_model

#################################################################
#                             MODEL 2                           #
#################################################################

class stoil_model2_cnn():
    
    def __init__(self):

    	self.name = 'model2_cnn'

    def model(self):
	    # Declaring the model
	    my_model = Sequential()
	    
	    # CNN input layer 1
	    my_model.add(Conv2D(32,(5,5), activation='relu', input_shape=(75,75,2), padding='same'))
	    # Max pool layer 1
	    my_model.add(MaxPooling2D(pool_size=(3,3), strides=4))
	    # Dropout for regularization
	    my_model.add(Dropout(0.25))
	    
	    # CNN layer 2
	    my_model.add(Conv2D(32,(5,5), activation='relu', padding='same'))
	    # Max pool layer 2
	    my_model.add(MaxPooling2D(pool_size=(3,3), strides=4))
	    # Dropout for regularization
	    my_model.add(Dropout(0.25))

	    # fully connected dense layers
	    my_model.add(Flatten())
	    my_model.add(Dense(32, activation='relu'))
	    my_model.add(Dropout(0.5))
	    my_model.add(Dense(2, activation='softmax'))
	    return my_model


#################################################################
#                             MODEL 3                           #
#################################################################
# Ref : Chen, S., Wang, H., Xu, F. and Jin, Y.Q., 2016. Target classification using the deep convolutional networks for SAR images. IEEE Transactions on Geoscience and Remote Sensing, 54(8), pp.4806-4817.
class stoil_model3_cnn():
    
    def __init__(self):

    	self.name = 'model3_cnn'

    def model(self):
	    # Declaring the model
	    my_model = Sequential()
	    
	    # CNN input layer 1
	    my_model.add(Conv2D(16,(5,5), activation='relu', input_shape=(75,75,2), padding='same'))
	    # Max pool layer 1
	    my_model.add(MaxPooling2D(pool_size=(2,2), strides=2))
	    
	    # CNN layer 2
	    my_model.add(Conv2D(32,(5,5), activation='relu', padding='same'))
	    # Max pool layer 2
	    my_model.add(MaxPooling2D(pool_size=(2,2), strides=2))
	    # Dropout for regularization
	     

	    # CNN layer 3
	    my_model.add(Conv2D(32,(6,6), activation='relu', padding='same'))
	    # Max pool layer 3
	    my_model.add(MaxPooling2D(pool_size=(2,2), strides=2))
	    # Dropout for regularization
	   
	    # CNN layer 4
	    my_model.add(Conv2D(3,(5,5), activation='relu', padding='same'))
	     # Dropout for regularization
	    my_model.add(Dropout(0.50))

	    # Dense layer
	    my_model.add(Flatten())
	    my_model.add(Dense(2, activation='softmax'))
	    return my_model



#################################################################
#                             MODEL 4                           #
#################################################################

class stoil_model1_nn():
    
    def __init__(self):

    	self.name = 'model1_nn'

    def model(self):
	    # Declaring the model
	    my_model = Sequential()
	    
	    # fully connected dense layers
	    my_model.add(Flatten())
	    my_model.add(Dense(4, activation='relu', input_shape=(75*75*2)))
	    my_model.add(Dropout(0.5))
	    my_model.add(Dense(2, activation='softmax'))
	    return my_model



#################################################################
#                             MODEL 5                           #
#################################################################
# Ref : Chen, S., Wang, H., Xu, F. and Jin, Y.Q., 2016. Target classification using the deep convolutional networks for SAR images. IEEE Transactions on Geoscience and Remote Sensing, 54(8), pp.4806-4817.
class stoil_model1_full_cnn():
    
    def __init__(self):

    	self.name = 'model1_full_cnn'

    def model(self):
	    # Declaring the model
	    my_model = Sequential()
	    
	    # CNN input layer 1
	    my_model.add(Conv2D(4,(5,5), activation='relu', input_shape=(75,75,2), padding='same'))
	    # Max pool layer 1
	    my_model.add(MaxPooling2D(pool_size=(2,2), strides=2))
	    
	    # CNN layer 2
	    my_model.add(Conv2D(8,(5,5), activation='relu', padding='same'))
	    # Max pool layer 2
	    my_model.add(MaxPooling2D(pool_size=(2,2), strides=2))
	    # Dropout for regularization
	    my_model.add(Dropout(0.20))

	    # CNN layer 3
	    my_model.add(Conv2D(16,(6,6), activation='relu', padding='same'))
	    # Max pool layer 3
	    my_model.add(MaxPooling2D(pool_size=(2,2), strides=2))
	    # Dropout for regularization
	    my_model.add(Dropout(0.20))
	    
	    # CNN layer 4
	    my_model.add(Conv2D(32,(5,5), activation='relu', padding='same'))
	    # Max pool layer 4
	    my_model.add(MaxPooling2D(pool_size=(2,2), strides=2))
	    # Dropout for regularization
	    my_model.add(Dropout(0.25))

	    # CNN layer 5
	    my_model.add(Conv2D(64,(5,5), activation='relu', padding='same'))
	    # Max pool layer 5
	    my_model.add(MaxPooling2D(pool_size=(4,4), strides=2))
	    # Dropout for regularization
	    my_model.add(Dropout(0.50))

	    # CNN layer 6
	    my_model.add(Conv2D(2,(5,5), activation='softmax', padding='same'))
	    my_model.add(Flatten())
	    return my_model