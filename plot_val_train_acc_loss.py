import os
import pandas as pd

from matplotlib import pyplot as plt

# This is to remove runtime error due to lack of display screen when running from shell
plt.switch_backend('agg')

# gets full name of directory the script resides in
dir_name = os.path.dirname(__file__) 

# Folders in current directory which we need
plot_save_path = r'acc_loss_plots'
path_to_log_file = r'logs'

# accessing and reading the log file for the given model 
log_name = r'model1_full_cnn_da_1_std_1_f_1_ep_200_bs_64_esp_30.log' # name of the log file you want to access
model_info = pd.read_csv(os.path.join(dir_name, path_to_log_file, log_name))

'''
epoch,acc,loss,val_acc,val_loss -> header names
'''

fig_name_short , _ = os.path.splitext(log_name)

# plot the val loss and train loss
plot = model_info.plot(x='epoch', y=['loss','val_loss'])
fig = plot.get_figure()

fig_file = os.path.join(dir_name, plot_save_path, fig_name_short + '_loss' + '.jpg')
fig.savefig(fig_file)



# plot val and train acc
plot = model_info.plot(x='epoch', y=['acc','val_acc'])
fig = plot.get_figure()

fig_file = os.path.join(dir_name, plot_save_path , fig_name_short + '_acc' + '.jpg')
fig.savefig(fig_file)
