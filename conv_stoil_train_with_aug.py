
# coding: utf-8

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Import necessary libraries
import os

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.callbacks import CSVLogger, EarlyStopping, ModelCheckpoint
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.optimizers import Adadelta
from keras.utils import np_utils
from keras.utils import plot_model
from sklearn.model_selection import train_test_split
from sklearn.externals import joblib

from utilities_stoil import *
from my_models import *

# Setting the random number generator so that the results are reproducible
np.random.seed(123)

# get the directory in which script resides
dirname = os.path.dirname(__file__)

# Some general options for the below code
is_data_aug = False
is_stdize = False
is_stdize_full = True

loss='categorical_crossentropy'
optimizer='adam'
batch_size = 64
epochs = 200
verbose_in_fit = 1
early_stopping_patience = 30

adadelta = Adadelta(lr=0.95, rho=0.95, epsilon=1e-08, decay=0.0)

# details of the model which are appended to the log name and model name 
append_to_model_name =    '_da_'  + str(int(is_data_aug)) \
					    + '_std_' + str(int(is_stdize)) \
					    + '_f_'   + str(int(is_stdize_full)) \
                        + '_ep_'  + str(epochs) \
                        + '_bs_'  + str(batch_size) \
                        + '_esp_' + str(early_stopping_patience)

#print(append_to_model_name)

# The path to the .json file with the training data
train_file = r"../train_data/train.json" 

# Pre-processing and extracting data from the .json file
train_data = ready_data(train_file,mode='train')

X_data = train_data['X_data']
X_data = X_data.reshape(X_data.shape[0],75,75,2)
y_data = train_data['Y_data']

# Splitting training data into training and validation data (stratified cross validation)
val_per = 0.30 # ratio of training data to be taken for validation
X_train, X_val, y_train, y_val = train_test_split(X_data, y_data, 
												   test_size=val_per,
												   stratify=y_data) 

# val_sz = int(X_train.shape[0] * val_per)
# train_sz = X_train.shape[0] - val_sz # resulting training data size after taking some for validation

print('The Validation data shape : ', X_val.shape)
print('The Validation labels shape : ', y_val.shape)

print('\n')
print('Data info augmentation : ')
print("X_train.shape : ",X_train.shape)
print("y_train.shape : ",y_train.shape)
print(type(y_train))
print(type(y_train[0]))

# Defining the functions and objects for data augmetnation
if is_data_aug:
	# defining the keras datagen object 
	datagen = ImageDataGenerator(rotation_range=90,
							     width_shift_range=0.3,
							     height_shift_range=0.3,
							     zoom_range=0.3,
							     horizontal_flip=True,
							     vertical_flip=True,
							     fill_mode='reflect')

	# setting parameters , Note: the total augmented dataset size = (total_batches*batch_size)
	total_batches = 10 
	batch_size = 150
	X_train_aug, y_train_aug = data_aug(X_train=X_train,
								y_train=y_train,
								datagen=datagen,
								total_batches=total_batches,
								batch_size=batch_size)

	X_train = np.concatenate((X_train,X_train_aug), axis=0)
	y_train = np.concatenate((y_train,y_train_aug), axis=0)


	print('\n')
	print('Data info after augmentation : ')
	print("X_train.shape : ", X_train.shape)
	print("y_train.shape : ", y_train.shape)



# Preprocess class labels and make then so that we get two labels [col_is_1_if_ship , col_is_1_if_iceberg]
Y_train = np_utils.to_categorical(y_train, num_classes=None)
Y_val = np_utils.to_categorical(y_val, num_classes=None)

# Declaring the model
stoil_cnn_class = stoil_model1_full_cnn()
stoil_cnn = stoil_cnn_class.model()

# Compile model
stoil_cnn.compile(loss=loss,
                  optimizer='adam',
                  metrics=['accuracy'])
# Print the model summary
stoil_cnn.summary()
#quit()

# Use early stopping w.r.t val_loss to stop model from overfitting or needlessly training (when bad model)
early_stopping = EarlyStopping(monitor='val_loss', 
							   patience=early_stopping_patience)


# Save the model accuracy and loss
log_path = 'logs'
log_name = stoil_cnn_class.name + append_to_model_name + '.log'
log_full_path = os.path.join(dirname, 
	                         log_path, 
	                         log_name)
csv_logger_call = CSVLogger(log_full_path)


# Save model for testing purposes
save_model_to_path = 'models'
save_model_name = stoil_cnn_class.name + append_to_model_name + '.h5'
save_model_full_path = os.path.join(dirname,
	                                save_model_to_path,
	                                save_model_name)
model_chkpt_call = ModelCheckpoint(save_model_full_path,
	                               monitor='val_acc',
	                               save_best_only=True)


# Normalize the training and validation images w.r.t mean and std dev before training
if is_stdize:
	X_train[:,:,:,0], scaler_band1 = stdize(X_train[:,:,:,0], scaler=True) # scales all samples stddev using mean and stddev of full training data
	X_train[:,:,:,1], scaler_band2 = stdize(X_train[:,:,:,1], scaler=True) # scales all samples stddev using mean and stddev of full training data

	# NOTE :  The scaler objects expect the size of their inputs to be 2 dim 
	#         such that shape = (no__of_samples, flattened_sample_size)
	
	# Standardizing the validation data w.r.t 
	X_val_band1 = X_val[:,:,:,0].reshape(X_val.shape[0],-1)
	scaler_band1.transform(X_val_band1)
	X_val[:,:,:,0] = X_val_band1.reshape(X_val.shape[0],75,75)

	X_val_band2 = X_val[:,:,:,1].reshape(X_val.shape[0],-1)
	scaler_band2.transform(X_val_band2)
	X_val[:,:,:,1] = X_val_band2.reshape(X_val.shape[0],75,75)

	# Store scalers for testing phase
	save_scaler_to_path = 'scalers'
	save_scaler_name = stoil_cnn_class.name + append_to_model_name
	save_scaler_full_path_b1 = os.path.join(dirname,
	                                     save_scaler_to_path,
	                                     save_scaler_name + '_band1.pkl') 

	save_scaler_full_path_b2 = os.path.join(dirname,
	                                     save_scaler_to_path,
	                                     save_scaler_name + '_band2.pkl') 
	
	joblib.dump(scaler_band1, save_scaler_full_path_b1)
	joblib.dump(scaler_band2, save_scaler_full_path_b2)
	print('Scalers saved : ', '\n', save_scaler_full_path_b1, '\n', save_scaler_full_path_b2)
	 

# Train model
stoil_cnn.fit(X_train, Y_train,
              batch_size=batch_size,
              epochs=epochs,
              verbose=verbose_in_fit,
              callbacks=[csv_logger_call, model_chkpt_call, early_stopping],
              validation_data=(X_val, Y_val))

print('Model saved : ', save_model_full_path)
print('Training Log saved : ', log_full_path)


'''
# Following code doenst work due to pydot installation issues in miniconda environment
# Save an image of the model 
save_viz_to_path = save_model_to_path
plot_model(stoil_cnn, 
	       to_file=save_viz_to_path + stoil_cnn_class.name + '.png', 
	       show_shapes=True)
'''