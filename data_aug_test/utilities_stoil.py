
import pandas as pd
import numpy as np

def ready_data_json(path_json_data, mode):
    '''

        path_json_data - a str object to the path of the data .json file
        mode - a string which specifies if data is training data of test data

    '''
    # read json file to pandas dataframe
    data_file = path_json_data
    print(mode + " data is in file : " + data_file)
    df_data = pd.read_json(data_file)

    print('setting up data dictionary...')
    processed_data = {}
    processed_data['X_data'] = np.zeros((df_data.shape[0],75*75,2))
    processed_data['Y_data'] = np.zeros((df_data.shape[0]))
    processed_data['sample_ids'] = []
    processed_data['mode'] = mode
 
    print(processed_data['X_data'].shape)
    print(processed_data['Y_data'].shape)

    print('processing data...')
    for idx,row in df_data.iterrows():
        processed_data['X_data'][idx,:,0] = np.asarray(row['band_1'])
        processed_data['X_data'][idx,:,1] = np.asarray(row['band_2'])
    
        if mode == 'train':
            processed_data['Y_data'][idx] = np.asarray(row['is_iceberg'])   

        if mode == 'test':
            processed_data['sample_ids'].insert(idx,row['id']) 
    print('data processed')
    return processed_data

def ready_data_df(df_data, mode):
    '''

        path_json_data - a str object to the path of the data .json file
        mode - a string which specifies if data is training data of test data

    '''
    print('setting up data dictionary...')
    processed_data = {}
    processed_data['X_data'] = np.zeros((df_data.shape[0],75*75,2))
    processed_data['Y_data'] = np.zeros((df_data.shape[0]))
    processed_data['sample_ids'] = []
    processed_data['mode'] = mode
 
    print(processed_data['X_data'].shape)
    print(processed_data['Y_data'].shape)

    print('processing data...')
    for idx,row in df_data.iterrows():
        processed_data['X_data'][idx,:,0] = np.asarray(row['band_1'])
        processed_data['X_data'][idx,:,1] = np.asarray(row['band_2'])
    
        if mode == 'train':
            processed_data['Y_data'][idx] = np.asarray(row['is_iceberg'])   

        if mode == 'test':
            processed_data['sample_ids'].insert(idx,row['id']) 
    print('data processed')
    return processed_data

def toUint8Img(img_raw):
    img = img_raw - img_raw.min()
    img /= img.max()
    img *= 255
    img = img.astype(np.uint8)
    return img