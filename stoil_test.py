
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Import necessary libraries
import os
import csv

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from keras.models import load_model
from keras.utils import plot_model
from sklearn.externals import joblib

from utilities_stoil import *
from my_models import *

# gets full name of directory the script resides in
dirname = os.path.dirname(__file__)

# load the desired model to test
models_path = 'models'
model_name = 'model1_full_cnn_da_1_std_1_f_1_ep_200_bs_64_esp_30'
model_full_path = os.path.join(dirname, models_path, model_name + '.h5')
my_model = load_model(model_full_path)

print('Loaded model : ', model_name)
my_model.summary()
quit()

# Load the scalers used for data standardization
scaler_path = 'scalers'
scaler_name = model_name
scaler_full_path_b1 = os.path.join(dirname,
	                               scaler_path,
	                               scaler_name + '_band1.pkl')
scaler_band1 = joblib.load(scaler_full_path_b1) # loading the scaler for band 1


scaler_full_path_b2 = os.path.join(dirname,
	                               scaler_path,
	                               scaler_name + '_band2.pkl')
scaler_band2 = joblib.load(scaler_full_path_b2) # loading the scaler for band 2


# Load the test data
# The path to the .json file with the testing data
test_file = r"../test_data/test.json" 

# Preprocess testing data for model
processed_data = ready_data(test_file, mode='test')
X_test = processed_data['X_data']
sample_ids = processed_data['sample_ids']

# Standardize the test set using the scaler used during training
X_test = X_test.reshape(X_test.shape[0], 75, 75, 2)

X_test_band1 = X_test[:,:,:,0].reshape(X_test.shape[0],-1)
scaler_band1.transform(X_test_band1)
X_test[:,:,:,0] = X_test_band1.reshape(X_test.shape[0],75,75)

X_test_band2 = X_test[:,:,:,1].reshape(X_test.shape[0],-1)
scaler_band2.transform(X_test_band2)
X_test[:,:,:,1] = X_test_band2.reshape(X_test.shape[0],75,75)


# Open a file to store the results to be submitted to kaggle
file_handle = open('test_' + model_name + '.csv', 
                   'w+' ,
                   newline='')
file_writer = csv.writer(file_handle, delimiter=',')
file_writer.writerow(['id','is_iceberg']) # the headers for the submission


for idx in range(0, X_test.shape[0]):
    X_sample = X_test[idx,:,:,:]
    # X_sample.reshape(1,*X_sample)
    X_sample = X_sample[np.newaxis,...]

    test_predictions = my_model.predict(X_sample, batch_size=1)
    
    file_writer.writerow([sample_ids[idx], test_predictions[:,1][0]])
    
