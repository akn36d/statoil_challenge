from keras.preprocessing.image import ImageDataGenerator
from sklearn.preprocessing import scale as sk_scale
from sklearn.preprocessing import StandardScaler

import pandas as pd
import numpy as np

def ready_data(path_json_data, mode):
    '''
    Function takes in the parameters given below and readies the data to be input to given into the 
    model of desire

        params :
        path_json_data - a str object to the path of the data .json file
        mode - a string which specifies if data is training data of test data

        returns :
        processed_data - a dictionaty with the processed and readied data
                         has the following labels,
                         X_data     : the trainig data where each sample (band1 and band2) is of size (75*75,2)
                         Y_data     : the labels for the data (only available when mode='train')
                         sample_ids : the id names for a given sample (only available when mode='test')
                         mode       : the mode with which the function was called in 

    '''
    # read json file to pandas dataframe
    data_file = path_json_data
    print(mode + " data is in file : " + data_file)
    df_data = pd.read_json(data_file)

    print('setting up data dictionary...')
    processed_data = {}
    processed_data['X_data'] = np.zeros((df_data.shape[0],75*75,2))
    processed_data['Y_data'] = np.zeros((df_data.shape[0]))
    processed_data['sample_ids'] = []
    processed_data['mode'] = mode
 
    print(processed_data['X_data'].shape)
    print(processed_data['Y_data'].shape)

    print('processing data...')
    for idx,row in df_data.iterrows():
        processed_data['X_data'][idx,:,0] = np.asarray(row['band_1'])
        processed_data['X_data'][idx,:,1] = np.asarray(row['band_2'])
    
        if mode == 'train':
            processed_data['Y_data'][idx] = np.asarray(row['is_iceberg'])   

        if mode == 'test':
            processed_data['sample_ids'].insert(idx,row['id']) 
    print('data processed')
    return processed_data



def data_aug(X_train,y_train,datagen,total_batches,batch_size):
    '''
    Function takes in the parameters given below and gives back the augmented training dataset
    
        params :
        X_train - the processed training data where each sample is of size (75,75,2)
        y_Train - the processed training labels
        datagen - the keras.preprocessing.image.ImageDatagenerator object created with the desired augmentations
        total_batches - the number of batches of data to be generated
        batch_size    - the size of a single batch of data
                        NOTE: the total size of the generated data is (total_batches*batch_size)            

        returns :
        X_train_aug - the augmented training data 
        y_train_aug - the the labels corresponding to the augmented data
        

    '''
    X_train_img = np.zeros((X_train.shape[0],75,75,3))
    X_train_img[:,:,:,0:2] = X_train
    
    datagen.fit(X_train_img)
    
    training_data_sz = total_batches * batch_size
    
    X_train_aug = np.zeros((training_data_sz,75,75,2))
    y_train_aug = np.zeros((training_data_sz))
    
    batches_cntr = 0
    for X_batch,y_batch in datagen.flow(X_train_img,y_train, batch_size=batch_size):
        if batches_cntr <= total_batches:
            for idx in range(0,9):
                img_aug = X_batch[idx].reshape(75,75,3)
                X_train_aug[batches_cntr + idx,:,:,:] = img_aug[:,:,0:2]
                
                y_train_aug[batches_cntr + idx] = y_batch[idx]

            batches_cntr += 1
        else:
            break

    return X_train_aug , y_train_aug     


def stdize_no_scaler(X, is_stdize_full=True):
    '''
    Function to standardize one band of the input stoil data. 
    It changes the mean and variance of the data to 0 and 1 respectively.

        params :
        X - all the input data sample (all the samples are for one band of the stoil data, either band 1 or band 2)
        is_stdize_full - if set to True this will standardize the mean and stddev w.r.t the complete input data X
                         if set to False it will standardize each sample individually

        returns : 
        X - the standardized input

    '''
    if is_stdize_full:
        X_temp = sk_scale(X.reshape(X.shape[0],-1), axis=0) # scales all samples stddev using mean and stddev of full training data
        X = X_temp.reshape(X.shape)
    else:
        X_temp = sk_scale(X.reshape(X.shape[0],-1), axis=1) # scales all samples stddev using mean and stddev of the individaul sample
        X = X_temp.reshape(X.shape)
        
    return X


def stdize_with_scaler(X):
    '''
    Function to standardize one band of the input stoil data and return a transform object to apply
    same transform on the test data. It is better to use this than the stdize_no_scaler() function 
    defined above
    It changes the mean and variance of the data to 0 and 1 respectively.

        params :
        X - all the input data sample (all the samples are for one band of the stoil data, either band 1 or band 2)
            this will always standardize the mean and stddev w.r.t the complete input data X
                         
        returns : 
        X - the standardized input
        scaler - the scaler object to be used for transforming the test data during testing

    '''
    X_temp = X.reshape(X.shape[0],-1) # scales all samples stddev using mean and stddev of full training data
    scaler = StandardScaler().fit(X_temp)
    scaler.transform(X_temp)
    X = X_temp.reshape(X.shape)

    return X, scaler


def stdize(X, scaler=True  ,**kwargs):
    '''
    Wrapper function for the standardization methods defined abeove
    '''
    if scaler:
        return stdize_with_scaler(X)
    else:
        is_stdize_full = kwargs.pop('is_stdize_full', True)
        return stdize_no_scaler(X, is_stdize_full)
