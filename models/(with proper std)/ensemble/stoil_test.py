
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Import necessary libraries
import os
import csv

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from keras.models import load_model
from keras.utils import plot_model
from sklearn.externals import joblib

from utilities_stoil import *

# load the desired model to test
model1_path = './model1_full_cnn_da_1_std_1_f_1_ep_200_bs_64_esp_30_adam/'
model2_path = './model2_cnn_da_0_std_1_f_1_ep_100_bs_128_esp_10_adam/' 
model3_path = './model2_cnn_da_0_std_1_f_1_ep_200_bs_128_esp_10_adam/'

# List the models 
model1_name = model1_path + 'model1_full_cnn_da_1_std_1_f_1_ep_200_bs_64_esp_30.h5'
model2_name = model2_path + 'model2_cnn_da_0_std_1_f_1_ep_100_bs_128_esp_10.h5' 
model3_name = model3_path + 'model2_cnn_da_0_std_1_f_1_ep_200_bs_128_esp_10.h5'

# Load all the models
model1 = load_model(model1_name)
model2 = load_model(model2_name)
model3 = load_model(model3_name)

# List out all scalers used for the ensemble
scaler_name_band1 = model2_path + 'model2_cnn_da_0_std_1_f_1_ep_100_bs_128_esp_10_band1.pkl'
scaler_name_band2 = model2_path + 'model2_cnn_da_0_std_1_f_1_ep_100_bs_128_esp_10_band2.pkl'

# Load the scalers used for data standardization (we use the scaling used in model2 (i.i one which had no data aug done to simplify the coding flow))
scaler_band1 = joblib.load(scaler_name_band1) # loading the scaler for band 1
scaler_band2 = joblib.load(scaler_name_band2) # loading the scaler for band 2


# Load the test data
# The path to the .json file with the testing data
test_file = r"./../../../../test_data/test.json" 

# Preprocess testing data for model
processed_data = ready_data(test_file, mode='test')
X_test = processed_data['X_data']
sample_ids = processed_data['sample_ids']


# Standardize the test set using the scaler used during training
X_test = X_test.reshape(X_test.shape[0], 75, 75, 2)

X_test_band1 = X_test[:,:,:,0].reshape(X_test.shape[0],-1)
scaler_band1.transform(X_test_band1)
X_test[:,:,:,0] = X_test_band1.reshape(X_test.shape[0],75,75)

X_test_band2 = X_test[:,:,:,1].reshape(X_test.shape[0],-1)
scaler_band2.transform(X_test_band2)
X_test[:,:,:,1] = X_test_band2.reshape(X_test.shape[0],75,75)


# Open a file to store the results to be submitted to kaggle
file_handle = open('test_' + 'ensemble' + '.csv', 'w+', newline='')
file_writer = csv.writer(file_handle, delimiter=',')
file_writer.writerow(['id','is_iceberg']) # the headers for the submission


for idx in range(0, X_test.shape[0]):
    X_sample = X_test[idx,:,:,:]
    
    X_sample = X_sample[np.newaxis,...]

    test_predictions1 = model1.predict(X_sample, batch_size=1)[:,1][0]
    test_predictions2 = model2.predict(X_sample, batch_size=1)[:,1][0]
    test_predictions3 = model3.predict(X_sample, batch_size=1)[:,1][0]
    test_predictions = (test_predictions1 + test_predictions2 + test_predictions3)/3.0
    file_writer.writerow([sample_ids[idx], test_predictions])







    
